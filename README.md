# urbancircus-moderation

1. Add `moderation` to `INSTALLED_APPS`
2. Run `python manage.py migrate`

On each model you wish to moderate, import and add the
`moderation.models.ModeratedModel` class to the base class of your model (you
may remove `models.Model` as `ModeratedModel` inherits from it already):

```
from moderation.models import ModeratedModel

class Person(ModeratedModel):
    pass
```

To add support for moderation in the model's admin, import and add
`moderation.admin.ModeratedAdmin` class to the base classes of your
`ModelAdmin` (you may remove `admin.ModelAdmin` as `ModeratedAdmin` inherits
from it already):

```
from moderation.admin import ModeratedAdmin

class PersonAdmin(ModeratedAdmin):
    pass
```
