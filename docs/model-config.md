# Moderated Model configuration

There are some configuration attributes that you may set on a moderated model
that control the behaviour of the moderation system.

## `moderation_include_related` : list

This list can be set to contain related models to moderate along with the
parent object. For instance if you inline a model `B`, you may set this
on model `A` so any approval or reject actions on `A` cascade to `B`.

Defaults to `[]`: no related models are included.

## moderation_excluded : list

This is a list of fields on the model to not perform moderation for. Any
fields listed here will bypass moderation when changed and will be saved as
usual. Useful for fields that are set by the system or change views that are
not user-editable.

Defaults to `[]`: no fields are excluded.

## moderation_approvals_queue_ignore : boolean

This toggle controls if the model's moderated objects appear in the approvals
queue list. Typically this would be set to `True` on relations that are included
in the `moderation_include_related` list.

Defaults to `False`: model's moderated objects are included in approvals queue.

## `created_by` : property str

By default moderation does not know (or care) who created a moderated object.
However there are places in the moderation views that can display this field.
Override the property in the moderated model to return the name of the user
who created the object.

Defaults to `None`.
