from django.conf import settings
from django.contrib import admin, messages
from django.contrib.admin.helpers import AdminForm
from django.contrib.admin.utils import flatten_fieldsets
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import PermissionDenied, ObjectDoesNotExist
from django.db.models import Q
from django.db.models.fields import DateTimeField, TextField
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse, path
from django.utils.html import format_html, strip_tags, mark_safe
from django_summernote.widgets import SummernoteWidget
from itertools import chain

from .models import *
from projects.models import Project
from moderation import signals

def send_pending_signal(obj):
    """
    Send pending signal depending on which approval group is required to
    moderate.
    """

    if obj.moderated_object.status == ModeratedObject.MODERATION_DRAFT:
        return

    group = obj.moderated_object.current_approval_group
    if group == 'Editor':
        signals.moderation_pending_editor_signal.send(
            sender=obj.__class__,
            instance=obj,
            moderated_object=obj.moderated_object,
        )
    elif group == 'Approver':
        signals.moderation_pending_approver_signal.send(
            sender=obj.__class__,
            instance=obj,
            moderated_object=obj.moderated_object,
        )

def multistep_workflow_needs_auto_approve(request, obj, moderated_object):
    """
    Returns True if an object can skip straight to approval.
    This will happen if an Editor or Approver edits an existing approved object.
    """
    user_groups = obj.user_groups(request.user)
    return moderated_object.status == ModeratedObject.MODERATION_APPROVED and \
            ('Editor' in user_groups or 'Approver' in user_groups)

def multistep_workflow_save(request, obj, moderated_object, auto_approve=False):
    """
    Perform the multistep workflow transitions.
    """

    if '_save' in request.POST:
        group = moderated_object.current_approval_group
        user_groups = obj.user_groups(request.user)

        # Editors and Approvers editing an existing Approved object can bypass
        # the rest of the workflow and perform final approval.
        if auto_approve:
            # Make sure we set the approver and editor when auto_approve is used
            # This ensures the next time the object change view is opened, it can be re-published
            moderated_object.approver = request.user
            moderated_object.editor = request.user
            moderated_object.last_moderator = request.user
            moderated_object.save()
            moderated_object.refresh_from_db()
            moderated_object.approve(request, moderated_object.moderator_comments, send_signal=False)
            return

        # Creator submits to either Editor or Approver
        if group not in user_groups:
            send_pending_signal(obj)

        # Editor approves, Editor -> Approver transition
        if group == 'Editor' and group in user_groups:
            moderated_object.editor = request.user
            moderated_object.last_moderator = request.user
            moderated_object.save()

            signals.moderation_pending_approver_signal.send(
                sender=obj.__class__,
                instance=obj,
                moderated_object=obj.moderated_object,
            )

        # Approver approves, Approver -> published transition
        if moderated_object.can_approve(request.user):
            moderated_object.approver = request.user
            moderated_object.last_moderator = request.user
            moderated_object.save()
            moderated_object.refresh_from_db()

            moderated_object.approve(request, moderated_object.moderator_comments)

    elif '_reject' in request.POST:
        n = moderated_object.current_approval_group
        if n == 'Editor':
            moderated_object.editor = None
        elif n == 'Approver':
            moderated_object.approver = None

        moderated_object.last_moderator = request.user
        moderated_object.save()
        moderated_object.refresh_from_db()

        moderated_object.reject(request, moderated_object.moderator_comments)

class ModeratedStatusListFilter(admin.SimpleListFilter):
    title = 'Moderation status'
    parameter_name = 'moderation_status'

    def lookups(self, request, model_admin):
        return ModeratedObject.MODERATION_STATUS_CHOICES

    def queryset(self, request, queryset):
        if self.value():
            ids = [o.id for o in queryset.all() if o.moderated_object and o.moderated_object.status == self.value()]
            return queryset.filter(id__in=ids)

class ModeratedAdmin(admin.ModelAdmin):
    change_form_template = 'moderation/moderated_admin_change_form.html'

    # Disabled for BB-117 as sorting doesn't work
    #def get_list_display(self, request):
    #    """
    #    Show pending values on changelist instead of original by adding new
    #    list_display lookups for all model fields.
    #    """
    #
    #    def monkey_patch_in_pending_field(field_name):
    #        f = '_get_pending_field_' + field_name
    #        def func(self, instance):
    #            obj = instance.moderated_object.changed_object if instance.needs_moderation else instance
    #            return getattr(self, field_name, getattr(obj, field_name))
    #        func.__name__ = field_name
    #        setattr(self.__class__, f, func)
    #        return f
    #
    #    list_display = []
    #    model_fields = [f.name for f in self.model._meta.fields if f.name != self.model._meta.pk.name]
    #    for field in self.list_display:
    #        if field in model_fields:
    #            field = monkey_patch_in_pending_field(field)
    #        list_display.append(field)
    #
    #    return list_display

    def get_form(self, request, obj=None, **kwargs):
        """
        Prevent saving m2m data by replacing the _save_m2m() method
        with a no-op method if the object is not a draft.
        """
        form = super().get_form(request, obj, **kwargs)
        if obj and obj.moderated_object.status != ModeratedObject.MODERATION_DRAFT:
            def _save_m2m_nop(form):
                return
            form._save_m2m = _save_m2m_nop
        return form

    def save_model(self, request, obj, form, change):
        def gather_m2m_data(form):
            """
            Generate a dict of m2m fields -> PKs.
            """
            m2m_data = {}
            for f in chain(form.instance._meta.many_to_many, form.instance._meta.private_fields):
                if not hasattr(f, 'save_form_data'):
                    continue
                if form._meta.fields and f.name not in form._meta.fields:
                    continue
                if form._meta.exclude and f.name in form._meta.exclude:
                    continue
                if f.name in form.cleaned_data:
                    m2m_data[f.name] = list(form.cleaned_data[f.name].values_list('id', flat=True))
            return m2m_data

        # Save the instance
        super().save_model(request, obj, form, change)

        # Save any m2m data on the moderated object
        obj.moderated_object.changed_m2m_data = gather_m2m_data(form)
        obj.moderated_object.save()

        # Now that object is saved, change status if save as draft or duplicate action was used
        if request.method == 'POST' and \
                ('save_as_draft' in request.POST or '_saveasnew' in request.POST):
            obj.moderated_object.status = ModeratedObject.MODERATION_DRAFT
            obj.moderated_object.save()

    def add_view(self, request, form_url='', extra_context=None):
        def group_name(project, name):
            return {
                'Creator': project.creators_name or 'Creator',
                'Editor': project.editors_name or 'Editor',
                'Approver': project.approvers_name or 'Approver',
            }[name]

        submit_groups = {
            'One': 'Approver',
            'Multi': 'Editor',
        }

        projects_submit_groups = {}
        for p in Project.objects.all():
            # If Editor or Approver creates a new object, they can skip ahead
            if request.user in p.approvers.all():
                projects_submit_groups[p.id] = 'Publishing'
            elif request.user in p.editors.all():
                projects_submit_groups[p.id] = 'Approver'
            else:
                projects_submit_groups[p.id] = group_name(p, submit_groups[p.workflow_type])

        extra_context = extra_context or {}
        extra_context['projects_submit_groups'] = projects_submit_groups
        extra_context['next_approval_group'] = '??'
        extra_context['add_view'] = True

        # Override post_save_add to send a pending signal as add_view() doesn't
        # have the newly saved object.
        if request.method == 'POST':
            original_post_save_add = self.response_post_save_add
            def post_save_add(request, obj):
                if request.method == 'POST':
                    if request.user in obj.project.approvers.all():
                        # If an Approver creates, they can submit to publish
                        obj.moderated_object.editor = obj.moderated_object.approver = request.user
                        obj.moderated_object.save()
                        obj.moderated_object.refresh_from_db()
                        
                        # Make sure we only approve if we're not saving as draft or as new
                        if not 'save_as_draft' in request.POST and not '_saveasnew' in request.POST:
                            obj.moderated_object.approve(request, send_signal=False)

                    elif request.user in obj.project.editors.all():
                        # If an Editor creates, they must submit to Approvers
                        obj.moderated_object.editor = request.user
                        obj.moderated_object.save()
                        obj.moderated_object.refresh_from_db()
                        send_pending_signal(obj)
                    else:
                        # Creator creates
                        send_pending_signal(obj)
                return original_post_save_add(request, obj)
            self.response_post_save_add = post_save_add

        return super().add_view(request, form_url, extra_context)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        instance = get_object_or_404(self.model, pk=object_id)
        app_info = self.model._meta.app_label, self.model._meta.model_name

        extra_context = extra_context or {}
        extra_context['has_pending_changes'] = instance.has_pending_changes
        extra_context['moderated_admin_url'] = instance.moderated_object.admin_change_url
        extra_context['prev_approval_group'] = instance.moderated_object.prev_approval_group_name(request.user)
        extra_context['next_approval_group'] = instance.moderated_object.next_approval_group_name(request.user)
        extra_context['user_groups'] = ','.join(instance.user_groups(request.user))
        extra_context['is_draft'] = instance.moderated_object.status == ModeratedObject.MODERATION_DRAFT
        extra_context['can_approve'] = instance.moderated_object.can_approve(request.user)
        extra_context['can_progress'] = instance.moderated_object.can_progress(request.user)

        if request.method == 'GET':
            if instance.moderated_object.status == ModeratedObject.MODERATION_APPROVED:
                message = f'The changes to this {app_info[1]} have been approved by {instance.moderated_object.last_moderator}: ' \
                          f'{strip_tags(instance.moderated_object.moderator_comments)}'
                messages.success(request, message)

            elif instance.moderated_object.status == ModeratedObject.MODERATION_REJECTED:
                message = f'The changes to this {app_info[1]} have been rejected by {instance.moderated_object.last_moderator}: ' \
                          f'{strip_tags(instance.moderated_object.moderator_comments)}'
                messages.error(request, message)

        # Draft -> Pending transition
        if request.method == 'POST' and instance.moderated_object.status == ModeratedObject.MODERATION_DRAFT \
                and 'save_as_draft' not in request.POST:
            instance.moderated_object.status = ModeratedObject.MODERATION_PENDING
            instance.save()

        # Check if we need to auto-approve the instance after saving. This must
        # be called prior to saving as it needs the current moderated object
        # status.
        auto_approve = False
        if request.method == 'POST':
            auto_approve = multistep_workflow_needs_auto_approve(request, instance, instance.moderated_object)

        # Save the object - this will trigger moderation. We must refresh the object here
        # # as _moderated_object may have changed.
        response = super().change_view(request, object_id, form_url, extra_context)
        instance.refresh_from_db()

        # Perform multistep workflow transitions
        if request.method == 'POST' and instance.moderated_object.status != ModeratedObject.MODERATION_DRAFT:
            multistep_workflow_save(request, instance, instance.moderated_object, auto_approve=auto_approve)

        # If there pending changes, show those changes in the admin form
        # instead of original object
        if request.method == 'GET' and instance.has_pending_changes and not isinstance(response, HttpResponseRedirect):
            obj = instance.moderated_object.changed_object
            form = self.get_form(request, obj, change=False)(instance=obj)
            formsets, inline_instances = self._create_formsets(request, obj, change=True)

            # Set initial data on m2m fields to the pending change
            if instance.moderated_object.changed_m2m_data:
                for field_name, ids in instance.moderated_object.changed_m2m_data.items():
                    if field_name in form.initial:
                        model = getattr(obj, field_name).model
                        form.initial[field_name] = list(model.objects.filter(id__in=ids))

            if not self.has_change_permission(request, obj):
                readonly_fields = flatten_fieldsets(self.get_fieldsets(request, obj))
            else:
                readonly_fields = self.get_readonly_fields(request, obj)

            inline_formsets = self.get_inline_formsets(request, formsets, inline_instances, obj)

            # Show pending changes on inline formsets
            for related_formset in inline_formsets:
                for inline in related_formset:
                    if inline.original:
                        for field in inline.original._meta.fields:
                            if field.name in inline.form.initial and field.name != 'disruption':
                                pending = getattr(inline.original.moderated_object.changed_object, field.name)
                                inline.form.initial[field.name] = pending

            adminform = AdminForm(
                form,
                list(self.get_fieldsets(request, obj)),
                self.get_prepopulated_fields(request, obj),
                readonly_fields,
                model_admin=self,
            )

            response.context_data['original'] = obj
            response.context_data['adminform'] = adminform
            response.context_data['inline_admin_formsets'] = inline_formsets

            msg = f'Please note: you are viewing pending changes for this {app_info[1]}. You may edit these changes if you wish.'
            messages.warning(request, msg)

        return response

    def approval_status(self, instance):
        return instance.moderated_object.status if instance.moderated_object else None

    def has_delete_permission(self, request, obj=None):
        """
        To delete an object the following conditions apply:

        1. The user must have the can_delete permission OR
        2. The object is in Draft state and the user is the original creator.
        """
        can_delete = super().has_delete_permission(request, obj)
        if obj:
            can_delete = can_delete or \
                         (obj.moderated_object.status == 'Draft' and obj.created_by == request.user)
        return can_delete

    def has_change_permission(self, request, obj=None):
        if obj is None:
            return True
        current = obj.moderated_object.current_approval_group

        # TODO: Review - this may be hacky but gets around BB-211
        if obj.moderated_object.workflow_type == 'One':
            # Creators change if it's NOT pending.  Approvers can change if it IS pending
            if "Creator" in obj.user_groups(request.user) and obj.moderated_object.status != 'Pending':
                return True
            if "Approver" in obj.user_groups(request.user):
                return True
            return False
        else:
            # Hacky fix to workaround BB-217, where a creator can't edit a draft in multistep
            if "Creator" in obj.user_groups(request.user) and obj.moderated_object.status == 'Draft':
                return True

        return current is None or current in obj.user_groups(request.user)

    def has_view_permission(self, request, obj=None):
        return True

    def has_module_permission(self, request):
        return True

class ApprovalsQueueAdmin(admin.ModelAdmin):
    list_display = ['name_and_review_link', 'status_with_group', 'creator', 'created', 'updated']

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_view_permission(self, request, obj=None):
        from projects.models import Project
        users = Q(editors=request.user) | Q(approvers=request.user)
        visible_projects = Project.objects.filter(users).distinct()
        return visible_projects.count() > 0

    def name_and_review_link(self, obj):
        """
        As ApprovalsQueueAdmin registers a proxy model, we need to redirect
        the user to the concrete model's (ModeratedObject) change_form
        instead.
        """
        return format_html(
            '<a href="{}">{}</a>',
            obj.admin_change_url,
            str(obj.content_object),
        )
    name_and_review_link.short_description = 'Review link'

    def creator(self, obj):
        return obj.content_object.created_by

    def status_with_group(self, obj):
        status = dict(obj.MODERATION_STATUS_CHOICES)[obj.status]
        return f'{status} by {obj.current_approval_group_name()}'
    status_with_group.short_description = 'Status'

admin.site.register(ApprovalsQueue, ApprovalsQueueAdmin)

class ModeratedObjectAdmin(admin.ModelAdmin):
    list_display = ['content_object', 'content_type', 'object_id', 'created', 'updated', 'status']
    change_form_template = 'moderation/moderated_object_change_form.html'
    formfield_overrides = {
        TextField: {'widget': SummernoteWidget},
    }
    mark_safe = []

    def response_post_save_change(self, request, obj):
        # Remove the default changed successfully message
        storage = messages.get_messages(request)
        if len(storage._queued_messages) > 0:
            del storage._queued_messages[-1]
        return super().response_post_save_change(request, obj)

    def has_add_permission(self, request):
        return False

    def has_view_permission(self, request, obj=None):
        return True

    def has_change_permission(self, request, obj=None):
        if obj:
            return obj is not None and (obj.can_progress(request.user) or request.user.is_superuser)
        return False

    def change_view(self, request, object_id, form_url='', extra_context=None):
        moderated_object = get_object_or_404(ModeratedObject, pk=object_id)

        old_obj = moderated_object.content_object
        new_obj = moderated_object.changed_object

        ct = ContentType.objects.get_for_model(new_obj.__class__)
        app_info = ct.app_label, ct.model

        # Add a warning banner to the page if the user is viewing a stale
        # moderated object (not the latest)
        if moderated_object.id != old_obj._moderated_object.id:
            messages.warning(request, 'Note: this is a stale version and the changes may not be current.')

        def changes_for_object(obj):
            changes = {}
            moderated_object = obj.moderated_object

            for field in obj._meta.fields:
                if field.name in ['id', 'uuid']:
                    continue

                original = getattr(moderated_object.content_object, field.name)
                try:
                    pending = getattr(moderated_object.changed_object, field.name)
                except ObjectDoesNotExist:
                    # A FK does not exist any more
                    pending = None
                skip_changed = isinstance(field, DateTimeField)

                if field.name in self.mark_safe:
                    original = mark_safe(original)
                    pending = mark_safe(pending)

                changes[field.name] = {
                    'name': field.name,
                    'verbose_name': field.verbose_name,
                    'original': original,
                    'pending': pending,
                    'changed': original != pending if not skip_changed else False,
                }

            for field in obj._meta.many_to_many:
                original = getattr(moderated_object.content_object, field.name).all()
                pending_model = getattr(moderated_object.changed_object, field.name).model
                try:
                    pending_ids = moderated_object.changed_m2m_data[field.name]
                except KeyError:
                    # This usually means the schema changed while there was saved m2m data,
                    # so there is nothing we can do except ignore this field
                    continue
                pending = pending_model.objects.filter(id__in=pending_ids).all()

                changes[field.name] = {
                    'name': field.name,
                    'verbose_name': field.verbose_name,
                    'original': ', '.join([str(f) for f in original]),
                    'pending': ', '.join([str(f) for f in pending]),
                    'changed': set(original) != set(pending),
                }
            
            return changes

        # Compute related changes
        related_changes = {}
        for field_name in new_obj.moderation_include_related:
            for obj in getattr(new_obj, field_name).all():
                related_changes[obj] = changes_for_object(obj)

        extra_context = extra_context or {}
        extra_context['changes'] = changes_for_object(new_obj)
        extra_context['related_changes'] = related_changes

        #Check across all changes and see if there are any.  Flag an overall has_changes value
        has_changes = False
        for key, val in extra_context['changes'].items():
            if key == '_moderated_object' or key == 'geometry': #TODO: Remove this
                continue
            if val['changed'] is True:
                has_changes = True

        for rel_key, rel_val in extra_context['related_changes'].items():
            for key, val in rel_val.items():
                if key == '_moderated_object':
                    continue
                if val['changed'] is True:
                    has_changes = True

        extra_context['has_changes'] = has_changes
        extra_context['object_admin_url'] = reverse('admin:%s_%s_change' % app_info, args=(new_obj.pk,))
        extra_context['has_pending_changes'] = old_obj.has_pending_changes
        if hasattr(settings, 'MAPS_API_KEY'):
            extra_context['MAPS_API_KEY'] = settings.MAPS_API_KEY
        extra_context['object_name'] = ct.model
        extra_context['prev_approval_group'] = moderated_object.prev_approval_group_name(request.user)
        extra_context['next_approval_group'] = moderated_object.next_approval_group_name(request.user)
        extra_context['current_approval_group'] = moderated_object.current_approval_group_name
        extra_context['is_draft'] = moderated_object.status == ModeratedObject.MODERATION_DRAFT
        extra_context['can_approve'] = moderated_object.can_approve(request.user)
        extra_context['can_progress'] = moderated_object.can_progress(request.user)

        response = super().change_view(request, object_id, form_url, extra_context)

        if request.method == 'POST':
            moderated_object.refresh_from_db() # super().change_view() would have modified this
            multistep_workflow_save(request, old_obj, moderated_object)
            return redirect(extra_context['object_admin_url'])

        return response

admin.site.register(ModeratedObject, ModeratedObjectAdmin)

class PendingNotificationAdmin(admin.ModelAdmin):
    readonly_fields = ['moderated_object', 'pending_since', 'recipients']
    list_display = readonly_fields

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def recipients(self, obj):
        return ', '.join([u.email for u in obj.recipients])

admin.site.register(PendingNotification, PendingNotificationAdmin)
