from django.db.models import Q
from django.urls import reverse

from .models import ApprovalsQueue
from projects.models import Project

def moderation(request):
    if request.user.is_authenticated:
        approval_items = [o for o in ApprovalsQueue.objects.all() if o.can_progress(request.user)]
        project_ids = [o.content_object.project.id
                       for o in approval_items
                       if o.content_object and hasattr(o.content_object, 'project')]
        users = Q(editors=request.user) | Q(approvers=request.user)
        projects = Project.objects.filter(users, id__in=project_ids).distinct()
        approver_on_any = Project.objects.filter(users).exists()
        return {
            'moderation_approvals_queue_perm': approver_on_any,
            'moderation_approvals_queue_count': len(approval_items),
            'moderation_approvals_queue_url': reverse('admin:moderation_approvalsqueue_changelist'),
        }
    else:
        return {}
