from django.conf import settings
from django.contrib import admin
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.html import strip_tags
from urllib.parse import urljoin

__all__ = ['send_approved_email', 'send_rejected_email', 'send_pending_approval_email']

def build_absolute_url(rel):
    if settings.ABSOLUTE_URL_PREFIX:
        return urljoin(settings.ABSOLUTE_URL_PREFIX, rel)
    else:
        return rel

def _send_email(moderated_object, template, to_list, subject):
    signature = render_to_string('moderation/email/signature.txt', {
        'admin_site': admin.site,
        'moderated_object': moderated_object,
    })
    for to in to_list:
        context = {
            'moderated_object': moderated_object,
            'admin_change_url': build_absolute_url(moderated_object.admin_change_url),
            'object_change_url': build_absolute_url(moderated_object.content_object.object_change_url),
            'to': to,
            'signature': signature,
        }
        content = render_to_string(template, context).replace('\n', '<br>')
        if settings.DEBUG:
            print('moderation.email.send_email():', to, subject, content)
        else:
            send_mail(
                subject,
                strip_tags(content),
                settings.DEFAULT_FROM_EMAIL,
                [to],
                fail_silently=not settings.DEBUG,
                html_message=content,
            )

def send_approved_email(moderated_object, to_list):
    """
    Sends an approval email to the specified recipients.
    """
    title = moderated_object.content_object._meta.verbose_name.title()
    subject = f'{title} approved: {moderated_object.content_object}'
    return _send_email(moderated_object, 'moderation/email/approved.html', to_list, subject)

def send_rejected_email(moderated_object, to_list):
    """
    Sends an rejection email to the specified recipients.
    """
    title = moderated_object.content_object._meta.verbose_name.title()
    subject = f'{title} rejected: {moderated_object.content_object}'
    return _send_email(moderated_object, 'moderation/email/rejected.html', to_list, subject)

def send_pending_approval_email(moderated_object, to_list):
    """
    Sends an email to the specified recipients notifying them of a pending approval.
    """
    subject = f'For approval: {moderated_object.content_object}'
    return _send_email(moderated_object, 'moderation/email/pending.html', to_list, subject)

def send_pending_digest_email(pending_objects):
    signature = render_to_string('moderation/email/signature.txt', {
        'admin_site': admin.site,
    })

    included_objs = []

    mapping = {} #: mapping of {email: [pending1, pending2, ..]}
    for pending in pending_objects:
        for email in pending.recipients:
            mapping.setdefault(email, [])
            if pending not in mapping[email]:
                mapping[email].append(pending)
        if pending.recipients:
            included_objs.append(pending)

    for to, pending in mapping.items():
        subject = render_to_string('moderation/email/digest_subject.txt', {
            'pending_objects': pending,
        }).strip()
        context = {
            'pending_objects': pending,
            'approvals_queue_url': build_absolute_url(reverse('admin:moderation_approvalsqueue_changelist')),
            'to': to,
            'signature': signature,
        }
        content = render_to_string('moderation/email/digest_pending.html', context).replace('\n', '<br>')
        if settings.DEBUG:
            print('moderation.email.send_pending_digest_email():', to, subject, content)
        else:
            send_mail(
                subject,
                strip_tags(content),
                settings.DEFAULT_FROM_EMAIL,
                [to],
                fail_silently=not settings.DEBUG,
                html_message=content,
            )

    return included_objs
