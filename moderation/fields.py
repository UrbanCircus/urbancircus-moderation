from django.core import serializers
from django.db import models

class SerializedObjectField(models.TextField):
    def db_type(self, connection):
        return 'text'

    def _serialize(self, object):
        if not object:
            return ''
        return serializers.serialize('json', [object])

    def _deserialize(self, value):
        if value == '':
            return None
        d = next(serializers.deserialize('json', value, ignorenonexistent=True))
        #d.object.m2m_data = d.m2m_data
        return d.object

    def to_python(self, value):
        if value is None:
            return value
        return self._deserialize(value)

    def from_db_value(self, value, expression, connection):
        return self.to_python(value)

    def get_db_prep_value(self, value, connection, prepared=False):
        return str(value)

    def pre_save(self, model_instance, add):
        value = getattr(model_instance, self.attname, None)
        return self._serialize(value)
