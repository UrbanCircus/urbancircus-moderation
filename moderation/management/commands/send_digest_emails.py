from django.core.management.base import BaseCommand

from moderation.email import send_pending_digest_email
from moderation.models import PendingNotification

class Command(BaseCommand):
    help = 'Sends any pending notifications as a digest email and clears the queue.'

    def handle(self, *args, **options):
        pending = PendingNotification.objects.all()
        included = send_pending_digest_email(pending)
        for obj in included:
            obj.delete()
