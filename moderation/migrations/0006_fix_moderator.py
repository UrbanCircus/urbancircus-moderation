
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('moderation', '0005_add_creator_editor_approver_fields'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='moderatedobject',
            name='moderator',
        ),
        migrations.AddField(
            model_name='moderatedobject',
            name='last_moderator',
            field=models.ForeignKey(editable=False, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='moderatedobject',
            name='status',
            field=models.CharField(choices=[('Draft', 'Draft'), ('Pending', 'Approval Pending'), ('Approved', 'Approved'), ('Rejected', 'Approval Rejected')], default='Pending', editable=False, max_length=20),
        ),
    ]
