
import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('moderation', '0006_fix_moderator'),
    ]

    operations = [
        migrations.AddField(
            model_name='pendingnotification',
            name='recipients',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=100), default=list, size=None),
        ),
    ]
