from django.conf import settings
from django.contrib import messages
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.fields import JSONField, ArrayField
from django.db import models
from django.db.models import Q
from django.urls import reverse

from .fields import SerializedObjectField
from moderation import signals

__all__ = ['ModeratedObject', 'ApprovalsQueue', 'ModeratedModel', 'PendingNotification']

class ModeratedObjectManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().select_related('content_type')

class ModeratedObject(models.Model):
    MODERATION_DRAFT = 'Draft'
    MODERATION_PENDING = 'Pending'
    MODERATION_APPROVED = 'Approved'
    MODERATION_REJECTED = 'Rejected'
    MODERATION_STATUS_CHOICES = (
        (MODERATION_DRAFT, 'Draft'),
        (MODERATION_PENDING, 'Approval Pending'),
        (MODERATION_APPROVED, 'Approved'), # this has two sub states: Scheduled, Published
        (MODERATION_REJECTED, 'Approval Rejected'),
    )
    pending_states = [MODERATION_DRAFT, MODERATION_PENDING]
    create_states = [MODERATION_REJECTED, MODERATION_APPROVED]

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, editable=False)
    object_id = models.PositiveIntegerField(editable=False)

    content_object = GenericForeignKey()
    changed_object = SerializedObjectField(editable=False)
    changed_m2m_data = JSONField(default=dict, editable=False)

    status = models.CharField(max_length=20, choices=MODERATION_STATUS_CHOICES, editable=False,
        default=MODERATION_PENDING)

    created = models.DateTimeField(auto_now_add=True, editable=False)
    updated = models.DateTimeField(auto_now=True)
    
    last_moderator = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, 
        null=True, editable=False, related_name='+')
    moderator_comments = models.TextField(blank=True)

    creator = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL,
        null=True, editable=False, related_name='+')
    editor = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL,
        null=True, editable=False, related_name='+')
    approver = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL,
        null=True, editable=False, related_name='+')

    objects = ModeratedObjectManager()

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return f'Moderated object for {self.verbose_name}: {self.changed_object}'

    @property
    def admin_change_url(self):
        # We hardcode the reverse lookup here in case a proxy model tries to access
        # this property.
        return reverse('admin:moderation_moderatedobject_change', args=(self.pk,))

    @property
    def approve_url(self):
        return reverse('admin:moderation_moderatedobject_approve', args=(self.pk,))

    @property
    def reject_url(self):
        return reverse('admin:moderation_moderatedobject_reject', args=(self.pk,))

    @property
    def verbose_name(self):
        return self.content_object._meta.verbose_name.title()

    def approve(self, request, comments=None, message=True, send_signal=True):
        # Copy over pending change object to real one and save
        self.content_object = self.changed_object
        self.content_object.save(approved_save=True, skip_moderation=True)

        # Update any m2m fields on the model. If any objects or fields do not
        # exist anymore they will be silently ignored.
        if self.changed_m2m_data:
            for field_name, ids in self.changed_m2m_data.items():
                field = getattr(self.content_object, field_name, None)
                if field:
                    field.set(field.model.objects.filter(id__in=ids))

        # Update the moderated object itself
        self.moderator = request.user
        self.status = self.MODERATION_APPROVED
        if comments:
            self.moderator_comments = comments
        self.save()

        # Cascade the approval to any related fields
        for related_field in self.content_object.moderation_include_related:
            for related in getattr(self.content_object, related_field).all():
                if hasattr(related, 'moderated_object') and related.moderated_object:
                    related.moderated_object.approve(request, comments, message=False)

        if message:
            msg = f'You have approved the change to {self.verbose_name}: {self.changed_object}'
            messages.success(request, msg)

        # Send approved signal
        if send_signal:
            signals.moderation_approved_signal.send(
                sender=self.content_object.__class__,
                instance=self.content_object,
                moderated_object=self,
            )

    def reject(self, request, comments=None, message=True):
        # Update the moderated object
        self.moderator = request.user
        self.status = self.MODERATION_REJECTED
        if comments:
            self.moderator_comments = comments
        self.save()

        # Cascade the rejection to any related fields
        for related_field in self.content_object.moderation_include_related:
            for related in getattr(self.content_object, related_field).all():
                if hasattr(related, 'moderated_object') and related.moderated_object:
                    related.moderated_object.reject(request, comments, message=False)

        if message:
            msg = f'You have rejected the change to {self.verbose_name}: {self.changed_object}'
            messages.error(request, msg)

        # Work out which signal to send and send it.
        group = self.prev_approval_group(request.user)
        signal = {
            'Creator': signals.moderation_rejected_creator_signal,
            'Editor': signals.moderation_rejected_editor_signal,
        }.get(group, None)
        if signal:
            signal.send(
                sender=self.content_object.__class__,
                instance=self.content_object,
                moderated_object=self,
            )

    def add_pending_notification(self, recipients):
        # HACK: This is added to bypass a moderated object signal getting fired multiple times
        self.remove_pending_notification()

        p = PendingNotification(
            moderated_object=self,
            recipients=recipients,
        )
        p.save()

    def remove_pending_notification(self):
        return PendingNotification.objects.filter(moderated_object=self).delete()

    @property
    def workflow_forward(self):
        return self.status != ModeratedObject.MODERATION_REJECTED

    @property
    def workflow_type(self):
        if self.content_object is None:
            return None
        return self.content_object.moderation_type[0]

    @property
    def current_approval_group(self):
        if self.content_object.moderation_type[0] == 'One':
            return 'Approver' if not self.approver else None
        elif self.content_object.moderation_type[0] == 'Multi':
            if self.editor and self.approver:
                return None
            elif self.editor and not self.approver:
                return 'Approver' if self.workflow_forward else 'Editor'
            elif not self.editor and not self.approver:
                return 'Editor' if self.workflow_forward else 'Creator'

    def current_approval_group_name(self):
        return self.content_object.group_names.get(self.current_approval_group, None)

    def prev_approval_group(self, user):
        """
        Returns one of ('Creator', Editor', 'Approver') signifying
        which group was the previous to approve.
        A return of None means the item has already been approved.
        """

        if self.content_object.moderation_type[0] == 'One':
            return 'Creator'
        elif self.content_object.moderation_type[0] == 'Multi':
            if self.editor and self.approver:
                return None
            elif self.editor and not self.approver:
                return 'Creator' if 'Editor' in self.content_object.user_groups(user) else 'Editor'
            elif not self.editor and not self.approver:
                return 'Creator' if 'Editor' in self.content_object.user_groups(user) else None

    def prev_approval_group_name(self, user):
        return self.content_object.group_names.get(self.prev_approval_group(user), None)

    def next_approval_group(self, user):
        """
        Returns one of ('Creator', Editor', 'Approver') signifying
        which group next has to approve. Creator means it is in Draft status.

        Creator -> [Editor] -> Approver
        """

        if self.content_object.moderation_type[0] == 'One':
            return 'Approver'
        elif self.content_object.moderation_type[0] == 'Multi':
            if self.editor and self.approver:
                return None
            elif (self.editor or 'Editor' in self.content_object.user_groups(user)) and not self.approver:
                return 'Approver'
            elif not self.editor and not self.approver:
                return 'Editor'

    def next_approval_group_name(self, user):
        return self.content_object.group_names.get(self.next_approval_group(user), None)

    def can_progress(self, user):
        """
        Returns True if the user can progress to the next or previous approval action.
        """
        return self.current_approval_group in self.content_object.user_groups(user)

    def can_approve(self, user):
        """
        Returns True if the user can perform the final approval action.
        """
        approver = (self.current_approval_group is None or self.current_approval_group == 'Approver') and \
            'Approver' in self.content_object.user_groups(user)
        editor = self.status == ModeratedObject.MODERATION_APPROVED and \
            'Editor' in self.content_object.user_groups(user)
        return any([approver, editor])

class ApprovalsQueueManager(models.Manager):
    def get_queryset(self):
        # Get all unique content types in the moderated objects, excluding unwanted ones
        distinct_ct_ids = ModeratedObject.objects.order_by('content_type') \
                                                 .distinct('content_type') \
                                                 .values_list('content_type', flat=True)
        content_types = ContentType.objects.filter(id__in=distinct_ct_ids)
        excluded_ct_ids = set([
            o.id for o in content_types
            if getattr(o.model_class(), 'moderation_approvals_queue_ignore', False)
        ])
        content_types = content_types.exclude(id__in=excluded_ct_ids)

        # Select the latest moderated object for each of the content typed objects
        # that is pending or rejected.
        object_ids = Q()
        for ct in content_types.all():
            # TODO: Improve this to display rejected items just for the specific user.
            # Temporarily removed rejected
            ids = ct.model_class().objects.filter(_moderated_object__status__in=['Pending',]) \
                                          .values_list('_moderated_object__id', flat=True)
            object_ids |= Q(id__in=ids)

        return super().get_queryset().filter(object_ids)

class ApprovalsQueue(ModeratedObject):
    """
    A proxy model for ModeratedObject that allows us to register it in the
    admin with different funcionality.
    """

    objects = ApprovalsQueueManager()

    class Meta:
        proxy = True
        verbose_name = 'pending object'
        verbose_name_plural = 'Approvals queue'
        ordering = [] # we must remove ordering here for the distinct() query to work
        permissions = (
            ('view_approval_queue', 'Can view approval queue'),
        )

class DefaultModeratedModelManager(models.Manager):
    """
    Add this to your model for greater performance (by selecting the
    moderated object whenever SQL is generated for a ModeratedModel).

    ```
    class Foo(ModeratedModel):
        [fields here]
        ...

        objects = DefaultModeratedModelManager()
    ```
    """

    def get_queryset(self):
        return super().get_queryset().select_related('_moderated_object')

class ModeratedModel(models.Model):
    # The moderated objects for this model. The `_moderated_object` field will
    # trigger a database migration but is required for performance reasons. For
    # even greater performance, set `objects = DefaultModeratedModelManager()`
    # in your model, or add `_moderated_object` to a select_related call in
    # your model's default manager `get_queryset` method.
    moderated_objects = GenericRelation(ModeratedObject)
    _moderated_object = models.ForeignKey(ModeratedObject, on_delete=models.SET_NULL, null=True, editable=False)

    # Include these foreign related fields when moderating this model
    moderation_include_related = []

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        # must pop these here as we're calling super() save soon
        approved_save = kwargs.pop('approved_save', False)
        skip_moderation = kwargs.pop('skip_moderation', False)

        if not self.moderated_object or self.moderated_object.status == ModeratedObject.MODERATION_DRAFT:
            approved_save = True

        if not self.pk or approved_save:
            super().save(*args, **kwargs)
        else:
            excluded = getattr(self, 'moderation_excluded', ['_moderated_object'])
            if excluded:
                super().save(update_fields=excluded)

        if not skip_moderation:
            moderated_object = update_or_create_moderated_object(self)
            if moderated_object.id != self.moderated_object.id:
                self._moderated_object = moderated_object
                super().save(update_fields=['_moderated_object'])

    @property
    def moderated_object(self):
        if not self._moderated_object:
            self._moderated_object = self.moderated_objects.order_by('-id').first()
            if self.pk:
                super().save(update_fields=['_moderated_object'])
        return self._moderated_object

    @property
    def has_pending_changes(self):
        return self.moderated_object and self.moderated_object.status in \
            [ModeratedObject.MODERATION_PENDING, ModeratedObject.MODERATION_REJECTED]

    @property
    def object_change_url(self):
        return reverse(f'admin:{self._meta.app_label}_{self._meta.model_name}_change', args=(self.pk,))

    @property
    def created_by(self):
        """
        Override this property to allow moderation to display the creator of the object.
        """
        return None

def update_or_create_moderated_object(instance):
    """
    Updates or creates the moderated object relating to the provided instance.

    If a moderated object is in draft or pending, simply update the existing.
    However if the object has previously been rejected or approved, then
    create a new object to store these changes.
    """

    if not instance._moderated_object or instance._moderated_object.status in ModeratedObject.create_states:
        moderated_object = ModeratedObject(
            content_object=instance,
            creator=instance.updated_by,
        )
    else:
        moderated_object = instance._moderated_object

    moderated_object.changed_object = instance
    moderated_object.save()
    return moderated_object

class PendingNotification(models.Model):
    moderated_object = models.ForeignKey(ModeratedObject, on_delete=models.CASCADE)
    pending_since = models.DateTimeField(auto_now_add=True)
    recipients = ArrayField(
        models.CharField(max_length=100),
        default=list,
    )

    class Meta:
        ordering = ['-pending_since']

    def __str__(self):
        return f'Pending notification for {self.moderated_object} since {self.pending_since}'
