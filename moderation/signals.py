import django.dispatch


## CREATORS

# Sent when an object under moderation has been rejected to a Creator
moderation_rejected_creator_signal = django.dispatch.Signal(
    providing_args=['instance', 'moderated_object'],
)


## EDITORS

# Sent when an object under moderation has been submitted for approval to an Editor
moderation_pending_editor_signal = django.dispatch.Signal(
    providing_args=['instance', 'moderated_object'],
)

# Sent when an object under moderation has been rejected to an Editor
moderation_rejected_editor_signal = django.dispatch.Signal(
    providing_args=['instance', 'moderated_object'],
)


## APPROVERS

# Sent when an object under moderation has been submitted for approval to an Approver
moderation_pending_approver_signal = django.dispatch.Signal(
    providing_args=['instance', 'moderated_object'],
)


## PUBLISHING

# Sent when an object under moderation has been approved through the multistep workflow
moderation_approved_signal = django.dispatch.Signal(
    providing_args=['instance', 'moderated_object'],
)
