from setuptools import find_packages, setup

from moderation import __version__

setup(
    name='urbancircus-moderation',
    version=__version__,
    author='Urban Circus',
    author_email='sam@sjkwi.com.au',
    description='Drop-in moderation support for objects',
    install_requires=[
        'Django >= 2.1',
        'django-summernote',
    ],
    packages=find_packages(),
    include_package_data=True,
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
)
